"""Read .clstr files produced by the CD-HIT program."""

from enum import Enum, unique
from typing import IO, Iterator, List, NamedTuple, Optional

import pygenomics.common

from pygenomics_ext import error


@unique
class SequenceUnitType(Enum):
    """Amino-acid or nucleotide sequence?"""

    AMINOACID = 1
    NUCLEOTIDE = 2

    def __str__(self) -> str:
        if self == SequenceUnitType.AMINOACID:
            return "aa"
        assert self == SequenceUnitType.NUCLEOTIDE
        return "nt"

    @classmethod
    def of_string(cls, line: str) -> Optional["SequenceUnitType"]:
        """Convert a line to the sequence unit type.

        :param line: Line possibly encoding a sequence unit type.

        :return: Parsed sequence unit type if `line` encodes it, `None` otherwise.

        """
        if line == "aa":
            return cls.AMINOACID
        if line == "nt":
            return cls.NUCLEOTIDE
        return None


@unique
class HitDirection(Enum):
    """Sequence hit direction in a cluster."""

    POSITIVE = 1
    NEGATIVE = 2

    def __str__(self) -> str:
        if self == HitDirection.POSITIVE:
            return "+"
        assert self == HitDirection.NEGATIVE
        return "-"

    @staticmethod
    def of_string(line: str) -> Optional["HitDirection"]:
        """Convert a line to the sequence hit direction.

        :param line: Line possibly encoding the hit direction.

        :return: Parsed hit direction if `line` encodes it, `None` otherwise.

        """
        if line == "+":
            return HitDirection.POSITIVE
        if line == "-":
            return HitDirection.NEGATIVE
        return None


class Sequence(NamedTuple):
    """Member of a cluster."""

    number: int
    """Sequence number within a cluster."""
    length: int
    """Sequence length in corresponding units."""
    seq_unit: SequenceUnitType
    """Sequence length units."""
    name: str
    """Sequence name."""

    def __str__(self) -> str:
        return str.format(
            "{:d}\t{:d}{}, {:s}",
            self.number,
            self.length,
            self.seq_unit,
            self.name,
        )

    @staticmethod
    def of_list(parts: List[str]) -> Optional["Sequence"]:
        """Convert a line to the cluster sequence record.

        :param parts: Parts of a line possibly encoding a cluster sequence.

        :return: Parsed cluster sequence record if `parts` encodes it, `None` otherwise.

        """
        if len(parts) == 3:
            if str.endswith(parts[1], ","):
                try:
                    number = int(parts[0])
                    length = int(parts[1][:-3])
                except ValueError:
                    return None
                seq_type = SequenceUnitType.of_string(parts[1][-3:-1])
                if seq_type is not None:
                    return Sequence(number, length, seq_type, parts[2])
        return None


class Element(NamedTuple):
    """Cluster element."""

    seq: Sequence
    """Element sequence."""
    direction: HitDirection
    """Direction of the hit to the main sequence of a cluster."""
    identity: float
    """Identity percentage of the hit to the main sequence of a cluster."""

    def __str__(self) -> str:
        return str.format("{} at {}/{:.2f}%", self.seq, self.direction, self.identity)

    @staticmethod
    def of_string(line: str) -> Optional["Element"]:
        """Convert a line to the cluster element record.

        :param line: Line possibly encoding a cluster element.

        :return: Parsed cluster element record if `line` encodes it, `None` otherwise.

        """
        parts = str.split(line)
        if len(parts) != 5 or parts[3] != "at":
            return None

        seq = Sequence.of_list(parts[:3])
        if seq is not None:
            last_parts = pygenomics.common.split2(parts[4], "/")
            if last_parts is not None:
                direction = HitDirection.of_string(last_parts[0])
                if direction is not None:
                    if str.endswith(last_parts[1], "%"):
                        try:
                            identity = float(last_parts[1][:-1])
                        except ValueError:
                            return None
                        return Element(seq, direction, identity)
        return None


_CLUSTER_PREFIX = ">Cluster "
_CLUSTER_PREFIX_LEN = len(_CLUSTER_PREFIX)
_CLUSTER_FORMAT = _CLUSTER_PREFIX + "{:d}"


class Cluster(NamedTuple):
    """Cluster detected by CD-HIT."""

    number: int
    """Cluster number."""
    main_seq: Sequence
    """Main sequence of the cluster."""
    elements: List[Element]
    """Sequences that are elements of the cluster."""

    @staticmethod
    def is_number_line(line: str) -> bool:
        """Does the line designate the beginning of a cluster record?

        :param line: Line from a .clstr file.

        :return: Is `line` the cluster number line?

        """
        return str.startswith(line, _CLUSTER_PREFIX)

    @staticmethod
    def is_main_seq_line(line: str) -> bool:
        """Does the line correspond to the main sequence of a cluster?

        :param line: Line from a .clstr file.

        :return: Is `line` the main sequence line?

        """
        return str.endswith(line, "*")

    @staticmethod
    def parse_cluster_number(line: str) -> Optional[int]:
        """Parse the cluster number from a .clstr file.

        :param line: Line possibly encoding the cluster header line.

        :return: Cluster number if `line` is a proper cluster header
          line, `None` otherwise.

        """
        if not str.startswith(line, ">Cluster "):
            return None
        try:
            return int(line[_CLUSTER_PREFIX_LEN:])
        except ValueError:
            return None

    @staticmethod
    def parse_main_sequence(line: str) -> Optional[Sequence]:
        """Parse the main sequence line from a clstr file.

        :param line: Line possibly encoding the main sequence of a cluster.

        :return: Parsed sequence record if `line` properly encodes
          it, `None` otherwise.

        """
        parts = str.split(line)
        if len(parts) == 4 and parts[3] == "*":
            seq = Sequence.of_list(parts[:3])
            if seq is not None:
                return seq
        return None

    def number_to_str(self) -> str:
        """Get the cluster number line."""
        return str.format(_CLUSTER_FORMAT, self.number)

    def main_seq_to_str(self) -> str:
        """Get the main sequence line."""
        return str.format("{} *", self.main_seq)


@unique
class ReadingErrorType(Enum):
    """Types of errors that may occur while reading a .clstr file."""

    INCORRECT_CLUSTER_NUMBER = 1
    MISSING_MAIN_SEQUENCE_LINES = 2
    MULTIPLE_MAIN_SEQUENCE_LINES = 3
    INCORRECT_MAIN_SEQUENCE = 4
    INCORRECT_CLUSTER_ELEMENT = 5


class ReadingError(error.Error):
    """Raised if an incorrect cluster record is read from a .clstr file."""

    line: str
    error_type: ReadingErrorType

    def __init__(self, line: str, error_type: ReadingErrorType):
        super().__init__(self, line, error_type)
        self.line = line
        self.error_type = error_type


def read(stream: IO[str]) -> Iterator[Cluster]:
    """Iterate cluster records from a stream from a .clstr file.

    :note: The function produces a side effect by reading from
      `stream`. It raises :py:class:`IncorrectClusterRecord` if an
      incorrect record is read from the stream.

    :param stream: Stream representing a .clstr file.

    :return: Iterator of cluster records from `stream`.

    """

    rstripped_stream = map(str.rstrip, stream)

    try:
        line = next(rstripped_stream)
    except StopIteration:
        return

    stop_flag = False

    while not stop_flag:

        cluster_number = Cluster.parse_cluster_number(line)
        if cluster_number is None:
            raise ReadingError(line, ReadingErrorType.INCORRECT_CLUSTER_NUMBER)

        sequence_lines: List[str] = []
        for line in rstripped_stream:
            if Cluster.is_number_line(line):
                break
            list.append(sequence_lines, line)
        else:
            stop_flag = True

        def get_main_seq() -> Sequence:
            """Get the main sequence of the cluster."""
            main_seq_lines = [k for k in sequence_lines if Cluster.is_main_seq_line(k)]
            if not main_seq_lines:
                raise ReadingError("", ReadingErrorType.MISSING_MAIN_SEQUENCE_LINES)
            if len(main_seq_lines) > 1:
                raise ReadingError(
                    main_seq_lines[0], ReadingErrorType.MULTIPLE_MAIN_SEQUENCE_LINES
                )

            assert len(main_seq_lines) == 1
            main_seq = Cluster.parse_main_sequence(main_seq_lines[0])
            if main_seq is None:
                raise ReadingError(line, ReadingErrorType.INCORRECT_MAIN_SEQUENCE)
            return main_seq

        def get_elements() -> List[Element]:
            """Get the cluster elements."""
            elements = [
                (k, Element.of_string(k))
                for k in sequence_lines
                if not Cluster.is_main_seq_line(k)
            ]
            filtered_elements = [k for _, k in elements if k is not None]
            if len(filtered_elements) != len(elements):
                error_lines = [j for j, k in elements if k is None]
                raise ReadingError(
                    error_lines[0], ReadingErrorType.INCORRECT_CLUSTER_ELEMENT
                )
            return filtered_elements

        cluster_main_seq = get_main_seq()
        cluster_elements = get_elements()
        yield Cluster(cluster_number, cluster_main_seq, cluster_elements)


def write(stream: IO[str], cluster: Cluster) -> None:
    """Write a cluster record to a stream in the format of .clstr files.

    :note: The function produces a side effect by writing to `stream`.

    :param stream: Output stream.

    :param cluster: Cluster record to write to `stream`.

    """
    print(Cluster.number_to_str(cluster), file=stream)
    print(Cluster.main_seq_to_str(cluster), file=stream)
    for element in cluster.elements:
        print(str(element), file=stream)
