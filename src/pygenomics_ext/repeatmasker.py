"""Read .out files produced by the RepeatMasker program."""

from typing import List, NamedTuple, Optional, Tuple

import pygenomics.common


class Percentages(NamedTuple):
    """Sequence similarity percentages."""

    divergence: float
    """# Mismatches / (#Mismatches + #Matches + # Unaligned bases in the query)."""
    deleted: float
    """% Deleted base pairs in the query."""
    inserted: float
    """% Inserted base pairs in the query."""

    def __str__(self) -> str:
        return str.format(
            "{:.1f} {:.1f} {:.1f}", self.divergence, self.deleted, self.inserted
        )

    @staticmethod
    def of_list(parts: List[str]) -> Optional["Percentages"]:
        """Convert line parts to sequence the record of similarity percentages.

        :param parts: Parts of a line possibly encoding similarity percentages.

        :return: Parsed record of sequence similarity percentages if
          `parts` encodes it, `None` otherwise.

        """
        if len(parts) != 3:
            return None
        try:
            return Percentages(float(parts[0]), float(parts[1]), float(parts[2]))
        except ValueError:
            return None


class Match(NamedTuple):
    """Match position."""

    start: int
    """Starting position in the match."""
    end: int
    """Ending position in the match."""
    left: int
    """Number of base pairs past the ending position in the match."""


class QueryPart(NamedTuple):
    """Query sequence match."""

    sequence: str
    """Query sequence name."""
    match: Match
    """Query sequence match."""
    is_complement: bool
    """Is the match with the complement of the repeat sequence?"""

    def __str__(self) -> str:
        return str.format(
            "{:s} {:d} {:d} ({:d}) {:s}",
            self.sequence,
            self.match.start,
            self.match.end,
            self.match.left,
            "C" if self.is_complement else "+",
        )

    @staticmethod
    def of_list(parts: List[str]) -> Optional["QueryPart"]:
        """Convert line parts to the query part record.

        :param parts: Parts of a line possibly encoding the query part record.

        :return: Parsed query part record if `parts` encodes it, `None` otherwise.

        """
        if len(parts) != 5:
            return None

        try:
            start, end = int(parts[1]), int(parts[2])
        except ValueError:
            return None

        left_str = pygenomics.common.strip_prefix_suffix(parts[3], "(", ")")
        if left_str is None:
            return None

        try:
            left = int(left_str)
        except ValueError:
            return None

        if parts[4] in {"C", "+"}:
            is_complement = parts[4] == "C"
        else:
            return None

        return QueryPart(parts[0], Match(start, end, left), is_complement)


class RepeatPart(NamedTuple):
    """Repeat match."""

    rep_name: str
    """Repeat name."""
    rep_class: str
    """Repeat class."""
    rep_family: Optional[str]
    """Repeat family."""
    match: Match
    """Repeat sequence match."""

    def to_string(self, is_complement: bool) -> str:
        """Convert the repeat match to a string.

        :param is_complement: Is the match with the complement of the
          repeat sequence?

        :return: String representing the repeat match.

        """

        def match_to_string() -> str:
            """Convert the match part to a string."""
            return (
                str.format(
                    "({:d}) {:d} {:d}",
                    self.match.left,
                    self.match.end,
                    self.match.start,
                )
                if is_complement
                else str.format(
                    "{:d} {:d} ({:d})",
                    self.match.start,
                    self.match.end,
                    self.match.left,
                )
            )

        def repeat_class_family_to_string() -> str:
            """Convert the repeat class and family to a string."""
            return (
                str.format("{:s}/{:s}", self.rep_class, self.rep_family)
                if self.rep_family is not None
                else self.rep_class
            )

        return str.format(
            "{:s} {:s} {:s}",
            self.rep_name,
            repeat_class_family_to_string(),
            match_to_string(),
        )

    @staticmethod
    def of_string(parts: List[str], is_complement: bool) -> Optional["RepeatPart"]:
        """Convert line parts to the repeat match record.

        :param parts: Parts of a line possibly encoding the repeat
          match record.

        :return: Parsed repeat match record if `parts` encodes it,
          `None` otherwise.

        """
        if len(parts) != 5:
            return None

        def parse_repeat_class_family() -> Tuple[str, Optional[str]]:
            """Parse the repeat class and family from a combined repeat class."""
            class_parts = str.split(parts[1], "/", 1)
            return (class_parts[0], None if len(class_parts) == 1 else class_parts[1])

        rep_class, rep_family = parse_repeat_class_family()

        start_str = parts[4] if is_complement else parts[2]
        end_str = parts[3]
        left_str = parts[2] if is_complement else parts[4]

        left_stripped_str = pygenomics.common.strip_prefix_suffix(left_str, "(", ")")
        if left_stripped_str is None:
            return None

        try:
            return RepeatPart(
                parts[0],
                rep_class,
                rep_family,
                Match(int(start_str), int(end_str), int(left_stripped_str)),
            )
        except ValueError:
            return None


class Record(NamedTuple):
    """Repeat match record."""

    sw_score: int
    """Smith-Waterman score of the match."""
    perc: Percentages
    """Divergence percentages between the query and the repeat."""
    query: QueryPart
    """"Match to the query sequence."""
    repeat: RepeatPart
    """Match to the repeat sequnce."""
    match_id: Optional[int]
    """Match ID."""
    higher_scoring_match: bool
    """Is there any other match with a higher score?"""

    def __str__(self) -> str:
        return (
            str.format(
                "{:d} {} {} {:s}",
                self.sw_score,
                self.perc,
                self.query,
                RepeatPart.to_string(self.repeat, self.query.is_complement),
            )
            + (str.format(" {:d}", self.match_id) if self.match_id is not None else "")
            + (" *" if self.higher_scoring_match else "")
        )

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Convert a line to the repeat match record.

        :param line: Line possibly encoding a repeat match record.

        :return: Parsed repeat match record if `line` encodes it,
          `None` otherwise.

        """
        parts = str.split(line)
        if len(parts) not in {14, 15, 16}:
            return None

        try:
            sw_score = int(parts[0])
        except ValueError:
            return None

        def parse_last_fields() -> Tuple[Tuple[Optional[int], bool], bool]:
            """Parse the two last fields of the record."""
            if len(parts) == 14:
                return ((None, False), True)

            if len(parts) == 15:
                if parts[14] == "*":
                    return ((None, True), True)
                match_id = pygenomics.common.str_to_int(parts[14])
                return ((match_id, False), match_id is not None)

            assert len(parts) == 16
            match_id = pygenomics.common.str_to_int(parts[14])
            return ((match_id, True), match_id is not None and parts[15] == "*")

        perc = Percentages.of_list(parts[1:4])
        if perc is not None:
            query = QueryPart.of_list(parts[4:9])
            if query is not None:
                repeat = RepeatPart.of_string(parts[9:14], query.is_complement)
                if repeat is not None:
                    (match_id, higher_scoring_match), success_flag = parse_last_fields()
                    if success_flag:
                        return Record(
                            sw_score,
                            perc,
                            query,
                            repeat,
                            match_id,
                            higher_scoring_match,
                        )

        return None
