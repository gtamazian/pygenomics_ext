"""Routines for processing Illumina FASTQ files."""

from typing import List, NamedTuple, Optional, Tuple

import pygenomics.common
import pygenomics.fastq

UMI = Optional[Tuple[str, str]]


class ReadName(NamedTuple):
    """Composite name of an Illumina read."""

    instrument: str
    """Instrument ID."""
    run: int
    """Run number."""
    flowcell: str
    """Flowcell ID."""
    lane: int
    """Lane number."""
    tile: int
    """Tile number."""
    x_pos: int
    """X coordinate of a flowcell cluster."""
    y_pos: int
    """Y coordinate of a flowcell cluster."""
    umi: UMI
    """Unique Molecular Identifier (UMI)."""
    read_num: int
    """Read number."""
    filtered: bool
    """Is read filtered?"""
    control_number: int
    """Control bits presence."""
    read_index: str
    """Read index."""

    def __str__(self) -> str:
        return (
            str.format(
                "{:s}:{:d}:{:s}:{:d}:{:d}:{:d}:{:d}",
                self.instrument,
                self.run,
                self.flowcell,
                self.lane,
                self.tile,
                self.x_pos,
                self.y_pos,
            )
            + (
                str.format(":{:s}+{:s}", self.umi[0], self.umi[1])
                if self.umi is not None
                else ""
            )
            + " "
            + str.format(
                "{:d}:{:s}:{:d}:{:s}",
                self.read_num,
                "Y" if self.filtered else "N",
                self.control_number,
                self.read_index,
            )
        )

    @staticmethod
    def of_string(line: str) -> Optional["ReadName"]:
        """Parse an Illumina read name.

        :param line: Line possibly encoding an Illumina read name.

        :return: Parsed Illumina read name if `line` encodes it, `None` otherwise.

        """

        def parse_left(
            parts: List[str],
        ) -> Optional[Tuple[str, int, str, int, int, int, int, UMI]]:
            """Parse the left part of an Illumina read name."""
            if len(parts) not in {7, 8}:
                return None
            instrument = parts[0]
            run = pygenomics.common.str_to_int(parts[1])
            flowcell = parts[2]
            lane = pygenomics.common.str_to_int(parts[3])
            tile = pygenomics.common.str_to_int(parts[4])
            x_pos = pygenomics.common.str_to_int(parts[5])
            y_pos = pygenomics.common.str_to_int(parts[6])
            if len(parts) == 8:
                umi = pygenomics.common.split2(parts[7], "+")
                if umi is None:
                    return None
            else:
                umi = None
            if (
                run is None
                or lane is None
                or tile is None
                or x_pos is None
                or y_pos is None
            ):
                return None
            return (instrument, run, flowcell, lane, tile, x_pos, y_pos, umi)

        def parse_right(parts: List[str]) -> Optional[Tuple[int, bool, int, str]]:
            """Parse the right part of an Illumina read name."""
            if len(parts) == 4:
                read_num = pygenomics.common.str_to_int(parts[0])
                filtered = parts[1] == "Y" if parts[1] in {"Y", "N"} else None
                control_number = pygenomics.common.str_to_int(parts[2])
                index = parts[3]
                if read_num is None or filtered is None or control_number is None:
                    return None
                return (read_num, filtered, control_number, index)
            return None

        parts = str.split(line)
        if len(parts) == 2:
            left = parse_left(str.split(parts[0], ":"))
            right = parse_right(str.split(parts[1], ":"))
            if left is not None and right is not None:
                return ReadName(
                    left[0],
                    left[1],
                    left[2],
                    left[3],
                    left[4],
                    left[5],
                    left[6],
                    left[7],
                    right[0],
                    right[1],
                    right[2],
                    right[3],
                )
        return None
