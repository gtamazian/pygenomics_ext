"""Reading and processing Juicebox .assembly files."""

import functools
import itertools
import operator
from dataclasses import dataclass
from enum import Enum, auto
from typing import Iterable, List, NamedTuple, Optional, Tuple

from pygenomics_ext import error


class Error(error.Error):
    """Base class for exceptions related to Juicebox routines."""


class FeatureKind(Enum):
    """Kind of a fragment feature."""

    DEBRIS = auto()
    FRAGMENT = auto()
    GAP = auto()
    OVERHANG = auto()

    @staticmethod
    def of_string(line: str) -> Optional["FeatureKind"]:
        """Parse a feature kind value from a line."""
        if line == "debris":
            return FeatureKind.DEBRIS
        if line == "fragment":
            return FeatureKind.FRAGMENT
        if line == "gap":
            return FeatureKind.GAP
        if line == "overhang":
            return FeatureKind.OVERHANG
        return None

    def __str__(self) -> str:
        if self is FeatureKind.DEBRIS:
            return "debris"
        if self is FeatureKind.FRAGMENT:
            return "fragment"
        if self is FeatureKind.GAP:
            return "gap"
        assert self is FeatureKind.OVERHANG
        return "overhang"


class NumberedFeatureError(Error):
    """Indicates that a number was specified for a feature of an incorrect kind."""

    kind: FeatureKind
    number: Optional[int]

    def __init__(self, kind: FeatureKind, number: Optional[int]):
        super().__init__(kind, number)
        self.kind = kind
        self.number = number


@dataclass(frozen=True)
class Feature:
    """Feature of an assembly fragment."""

    kind: FeatureKind
    number: Optional[int]

    def __post_init__(self) -> None:
        if (self.kind is FeatureKind.DEBRIS or self.kind is FeatureKind.GAP) != (
            self.number is None
        ):
            raise NumberedFeatureError(self.kind, self.number)

    def __str__(self) -> str:
        return (
            str.format("{}_{:d}", self.kind, self.number)
            if self.number is not None
            else str(self.kind)
        )

    @staticmethod
    def of_string(line: str) -> Optional["Feature"]:
        """Parse a feature from a line."""
        number: Optional[int]
        if "_" in line:
            parts = str.split(line, "_", 1)
            assert len(parts) == 2
            kind = FeatureKind.of_string(parts[0])
            try:
                number = int(parts[1])
            except ValueError:
                return None
        else:
            kind = FeatureKind.of_string(line)
            number = None

        if kind is None:
            return None

        try:
            return Feature(kind, number)
        except NumberedFeatureError:
            return None


class Fragment(NamedTuple):
    """Hi-C assembly fragment."""

    seqname: str
    features: List[Feature]
    number: int
    size: int

    def __str__(self) -> str:
        return str.format(
            ">{:s} {:d} {:d}",
            str.join(":::", [self.seqname] + [str(k) for k in self.features]),
            self.number,
            self.size,
        )

    @staticmethod
    def of_string(line: str) -> Optional["Fragment"]:
        """Parse a fragment line."""
        if not str.startswith(line, ">"):
            return None
        parts = str.split(line[1:], " ", 2)
        if len(parts) != 3:
            return None
        seqname_parts = str.split(parts[0], ":::")
        seqname = seqname_parts[0]
        features = [Feature.of_string(k) for k in seqname_parts[1:]]
        filtered_features = [k for k in features if k is not None]
        if len(filtered_features) < len(features):
            return None
        try:
            number = int(parts[1])
            size = int(parts[2])
        except ValueError:
            return None
        return Fragment(seqname, filtered_features, number, size)


class Part(NamedTuple):
    """Part of an assembled sequence."""

    number: int
    is_reverse: bool

    @staticmethod
    def of_string(line: str) -> Optional["Part"]:
        """Parse the part from a line."""
        try:
            if str.startswith(line, "-"):
                return Part(int(line[1:]), True)
            return Part(int(line), False)
        except ValueError:
            return None

    def __str__(self) -> str:
        return (
            str.format("-{:d}", self.number)
            if self.is_reverse
            else str.format("{:d}", self.number)
        )


Sequence = List[Part]


def sequence_to_string(sequence: Sequence) -> str:
    """Convert a sequence to the string."""
    return str.join(" ", (str(k) for k in sequence))


def sequence_of_string(line: str) -> Optional[Sequence]:
    """Parse the sequence record from a line."""
    if not line:
        return []
    raw_sequence = [Part.of_string(k) for k in str.split(line, " ")]
    filtered_sequence = [k for k in raw_sequence if k is not None]
    if len(filtered_sequence) < len(raw_sequence):
        return None
    return filtered_sequence


class AssemblyReadingErrorType(Enum):
    """Types of errors that may occur while reading an .assembly file."""

    MISSING_GAP = auto()
    MISSING_FRAGMENT = auto()
    INCORRECT_LINE = auto()
    INCORRECT_LINE_ORDER = auto()


class AssemblyReadingError(Error):
    """Indicates an error while reading an .assembly file."""

    error_type: AssemblyReadingErrorType
    line: str

    def __init__(self, error_type: AssemblyReadingErrorType, line: str):
        super().__init__(error_type, line)
        self.error_type = error_type
        self.line = line


class Assembly(NamedTuple):
    """Genome assembly produced by Juicebox."""

    fragments: List[Fragment]
    sequences: List[Sequence]

    @staticmethod
    def read(path: str) -> "Assembly":
        """Read the assembly from a file."""
        with open(path, encoding="utf-8") as assembly_file:

            def read_fragments() -> Tuple[List[Fragment], str]:
                """Read the first file of the assembly file."""
                result: List[Fragment] = []
                frag_line = ""
                for frag_line in assembly_file:
                    if not str.startswith(frag_line, ">"):
                        break
                    current_fragment = Fragment.of_string(str.rstrip(frag_line))
                    if current_fragment is None:
                        raise AssemblyReadingError(
                            AssemblyReadingErrorType.INCORRECT_LINE, frag_line
                        )
                    list.append(result, current_fragment)
                if not str.startswith(result[-1].seqname, "hic_gap_"):
                    raise AssemblyReadingError(
                        AssemblyReadingErrorType.MISSING_GAP, str(result[-1])
                    )
                return result, frag_line

            fragments, last_line = read_fragments()
            seq_numbers = {k.number for k in fragments}

            def parse_sequence_line(seq_line: str) -> Sequence:
                """Parse an assembled sequence line with proper checks."""
                if str.startswith(seq_line, ">"):
                    raise AssemblyReadingError(
                        AssemblyReadingErrorType.INCORRECT_LINE_ORDER, seq_line
                    )
                result = sequence_of_string(str.rstrip(seq_line))
                if result is None:
                    raise AssemblyReadingError(
                        AssemblyReadingErrorType.INCORRECT_LINE, seq_line
                    )
                if any(k.number not in seq_numbers for k in result):
                    raise AssemblyReadingError(
                        AssemblyReadingErrorType.MISSING_FRAGMENT, seq_line
                    )
                return result

            sequences = [parse_sequence_line(last_line)] + [
                parse_sequence_line(line) for line in assembly_file
            ]
            return Assembly(fragments, sequences)


Interval = Tuple[str, int, int]


def get_fragment_intervals(fragments: List[Fragment]) -> List[Interval]:
    """Get intervals of Hi-C assembly fragments in the reference genome.

    The interval coordinates are zero-based and half-open, as in the BED format.

    """

    Acc = Tuple[List[Interval], int]

    def process_fragment(acc: Acc, new: Fragment) -> Acc:
        """Process a new assembly fragment."""
        processed, position = acc
        return (
            processed + [(new.seqname, position, position + new.size)],
            position + new.size,
        )

    def get_seq_intervals(seq_fragments: Iterable[Fragment]) -> List[Interval]:
        """Get intervals for fragments from the same sequence."""
        init: Acc = ([], 0)
        result, _ = functools.reduce(process_fragment, seq_fragments, init)
        return result

    return list(
        itertools.chain.from_iterable(
            get_seq_intervals(k)
            for _, k in itertools.groupby(
                sorted(fragments, key=operator.attrgetter("number")),
                key=operator.attrgetter("seqname"),
            )
        )
    )


class AssembledPart(NamedTuple):
    """Assembled genome part with the reference to an original assembly fragment."""

    interval: Interval
    fragment: int
    is_reverse: bool


def get_target_intervals(assembly: Assembly) -> List[AssembledPart]:
    """Get intervals of Hi-C assembly parts with references to an original assembly."""

    fragments = {k.number: k for k in assembly.fragments}

    def get_sequence_intervals(name: str, parts: List[Part]) -> List[AssembledPart]:
        """Get intervals for a Hi-C assembled sequence."""

        Acc = Tuple[List[AssembledPart], int]

        def process_part(acc: Acc, new: Part) -> Acc:
            """Process a new part of the assembled sequence."""
            processed, pos = acc
            list.append(
                processed,
                AssembledPart(
                    (name, pos, pos + fragments[new.number].size),
                    new.number,
                    new.is_reverse,
                ),
            )
            pos += fragments[new.number].size
            return (processed, pos)

        init: Acc = ([], 0)
        sequence_intervals, _ = functools.reduce(process_part, parts, init)
        return sequence_intervals

    return [
        j
        for k, assembled_sequence in enumerate(assembly.sequences, start=1)
        for j in get_sequence_intervals(
            str.format("HiC_scaffold_{:d}", k), assembled_sequence
        )
    ]
