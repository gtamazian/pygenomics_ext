"""Exceptions for package pygenomics_ext."""


class Error(Exception):
    """Base class for exceptions in the package."""
