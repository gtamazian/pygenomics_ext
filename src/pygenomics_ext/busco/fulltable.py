"""Reading files in the BUSCO full table format."""

import itertools
from enum import Enum, auto
from typing import List, NamedTuple, Optional

from pygenomics.common import str_to_float, str_to_int

from pygenomics_ext import error


class GeneStatus(Enum):
    """Annotated BUSCO gene status."""

    COMPLETE = auto()
    DUPLICATED = auto()
    FRAGMENTED = auto()
    MISSING = auto()

    def __str__(self) -> str:
        if self is GeneStatus.COMPLETE:
            return "Complete"
        if self is GeneStatus.DUPLICATED:
            return "Duplicated"
        if self is GeneStatus.FRAGMENTED:
            return "Fragmented"
        assert self is GeneStatus.MISSING
        return "Missing"

    @staticmethod
    def of_string(line: str) -> Optional["GeneStatus"]:
        """Parse the gene status from a line."""
        if line == "Complete":
            return GeneStatus.COMPLETE
        if line == "Duplicated":
            return GeneStatus.DUPLICATED
        if line == "Fragmented":
            return GeneStatus.FRAGMENTED
        if line == "Missing":
            return GeneStatus.MISSING
        return None


class Strand(Enum):
    """Annotated gene strand."""

    POSITIVE = auto()
    NEGATIVE = auto()

    def __str__(self) -> str:
        return "+" if self is Strand.POSITIVE else "-"

    @staticmethod
    def of_string(line: str) -> Optional["Strand"]:
        """Parse the gene strand from a line."""
        if line == "+":
            return Strand.POSITIVE
        if line == "-":
            return Strand.NEGATIVE
        return None


class Gene(NamedTuple):
    """Gene part of the annotated gene record."""

    scaffold: str
    start: int
    end: int
    strand: Strand
    score: float
    length: int
    url: str
    description: str

    def __str__(self) -> str:
        return str.format(
            "{:s}\t{:d}\t{:d}\t{}\t{:f}\t{:d}\t{:s}\t{:s}",
            self.scaffold,
            self.start,
            self.end,
            self.strand,
            self.score,
            self.length,
            self.url,
            self.description,
        )

    @staticmethod
    def of_list(parts: List[str]) -> Optional["Gene"]:
        """Parse the gene part from line parts of an annotated gene record."""
        if len(parts) == 8:
            start = str_to_int(parts[1])
            end = str_to_int(parts[2])
            strand = Strand.of_string(parts[3])
            score = str_to_float(parts[4])
            length = str_to_int(parts[5])
            if (
                start is not None
                and end is not None
                and strand is not None
                and score is not None
                and length is not None
            ):
                return Gene(
                    parts[0],
                    start,
                    end,
                    strand,
                    score,
                    length,
                    parts[6],
                    parts[7],
                )
        return None


class RecordError(error.Error):
    """Indicates an incorrect annotated gene record."""

    line: str

    def __init__(self, line: str):
        super().__init__(self, line)
        self.line = line


class Record(NamedTuple):
    """Record of a gene annotated by BUSCO."""

    busco_id: str
    status: GeneStatus
    gene: Optional[Gene]

    def __str__(self) -> str:
        return (
            str.format("{:s}\t{}", self.busco_id, self.status)
            if self.status is GeneStatus.MISSING
            else str.format("{:s}\t{}\t{}", self.busco_id, self.status, self.gene)
        )

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Parse the annotated gene record from a line."""
        parts = str.split(line, "\t")
        busco_id = parts[0]
        if len(parts) in {2, 10}:
            status = GeneStatus.of_string(parts[1])
            if status is not None:
                if len(parts) == 10:
                    gene = Gene.of_list(parts[2:])
                    if gene is not None:
                        return Record(busco_id, status, gene)
                return Record(busco_id, status, None)
        return None


def read(path: str) -> List[Record]:
    """Read the full table file of genes annotated by BUSCO."""
    with open(path, encoding="utf-8") as table_file:
        result: List[Record] = []
        for line in itertools.dropwhile(lambda x: str.startswith(x, "#"), table_file):
            current_record = Record.of_string(str.rstrip(line))
            if current_record is None:
                raise RecordError(line)
            list.append(result, current_record)
        return result
