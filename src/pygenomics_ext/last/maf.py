"""Reading MAF files of pairwise alignments produced by LAST."""

import itertools
from typing import IO, Iterator, List, NamedTuple, Optional, Tuple

import pygenomics.error
from pygenomics.strand import Strand


class AlignedFragment(NamedTuple):
    """Fragment of an aligned sequence from a LAST MAF file."""

    name: str
    start: int
    aln_len: int
    strand: Strand
    seq_len: int
    aln_seq: str

    def __str__(self) -> str:
        return str.format(
            "s {:s} {:d} {:d} {} {:d} {:s}",
            self.name,
            self.start,
            self.aln_len,
            self.strand,
            self.seq_len,
            self.aln_seq,
        )

    @staticmethod
    def of_string(line: str) -> Optional["AlignedFragment"]:
        """Parse an aligned sequence line from a LAST MAF file.

        :param line: Line describing an aligned sequence fragment.

        :return: Aligned sequence fragment if `line` properly describes it, `None` otherwise.

        """
        parts = str.split(line)
        if len(parts) == 7 and parts[0] == "s":
            try:
                start = int(parts[2])
                aln_len = int(parts[3])
                seq_len = int(parts[5])
            except ValueError:
                return None
            strand = Strand.of_string(parts[4])
            if strand is not None:
                return AlignedFragment(
                    parts[1], start, aln_len, strand, seq_len, parts[6]
                )
        return None


class Record(NamedTuple):
    """Alignment record in the LAST MAF format."""

    score: int
    extra: str
    target: AlignedFragment
    query: AlignedFragment

    def get_header_string(self) -> str:
        """Get the alignment header line."""
        return (
            str.format("a score={:d} {:s}", self.score, self.extra)
            if self.extra
            else str.format("a score={:d}", self.score)
        )


def parse_header(line: str) -> Optional[Tuple[int, str]]:
    """Parse the alignment header line.

    :param line: Line containing the alignment header.

    :return: Alignment score and extra fields if `line` properly
      encodes the header, `None` otherwise.

    """

    def get_score(score_line: str) -> Optional[int]:
        """Extract the score numeric value."""
        score_parts = str.split(score_line, "=", 1)
        if len(score_parts) == 2:
            try:
                return int(score_parts[1])
            except ValueError:
                return None
        return None

    parts = str.split(line, None, 2)
    if len(parts) < 2 or parts[0] != "a":
        return None
    score = get_score(parts[1])
    if score is None:
        return None

    return (score, parts[2] if len(parts) == 3 else "")


class MalformedRecord(pygenomics.error.Error):
    """Error indicating a malformed alignment record."""

    lines: List[str]

    def __init__(self, lines: List[str]):
        super().__init__(lines)
        self.lines = lines


def read(stream: IO[str]) -> Iterator[Record]:
    """Iterate alignments from a stream int the LAST MAF format.

    :note: The function produces a side effect by reading from
      `stream`. It raises :py:class:`MalformedRecord` if lines from
      `stream` form an incorrect alignment record.

    :param stream: Stream in the LAST MAF format.

    :return: Iterator of alignments from `stream`.

    """

    filtered_stream = filter(
        lambda x: x and not str.startswith(x, "#"), map(str.rstrip, stream)
    )

    for header_line, target_line, query_line in itertools.zip_longest(
        *[filtered_stream] * 3
    ):
        header = parse_header(header_line)
        target = AlignedFragment.of_string(target_line)
        query = AlignedFragment.of_string(query_line)
        if header is not None and target is not None and query is not None:
            yield Record(header[0], header[1], target, query)
        else:
            raise MalformedRecord([header_line, target_line, query_line])


def write(stream: IO[str], alignment: Record) -> None:
    """Write an alignment record to the stream in the LAST MAF format.

    :note: The function produces a side effect by writing to `stream`.

    :param stream: Output stream.

    :param alignment: Alignment to write to `stream`.

    """
    print(Record.get_header_string(alignment), file=stream)
    print(alignment.target, file=stream)
    print(alignment.query, file=stream)
