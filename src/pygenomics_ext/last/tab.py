"""Reading LAST pairwise alignments in the tabular (TAB) format."""

from typing import List, NamedTuple, Optional

from pygenomics.strand import Strand


class Region(NamedTuple):
    """Sequence region aligned by LAST."""

    seq: str
    start: int
    end: int
    strand: Strand
    seq_len: int

    def __str__(self) -> str:
        return str.format(
            "{:s}\t{:d}\t{:d}\t{}\t{:d}",
            self.seq,
            self.start,
            self.end,
            self.strand,
            self.seq_len,
        )

    @staticmethod
    def of_list(parts: List[str]) -> Optional["Region"]:
        """Parse the aligned region from LAST TAB line parts.

        :param parts: Parts of the alignment line that describe a
          target or query aligned region.

        :return: Aligned region if `parts` properly describe it,
          `None` otherwise.

        """
        if len(parts) == 5:
            try:
                start = int(parts[1])
                end = int(parts[2])
                seq_len = int(parts[4])
            except ValueError:
                return None
            strand = Strand.of_string(parts[3])
            if strand is not None:
                return Region(parts[0], start, end, strand, seq_len)
        return None


_RECORD_REQUIRED_COLUMNS = 11


class Record(NamedTuple):
    """Alignment record in the tabular LAST format."""

    score: int
    target: Region
    query: Region
    extra: str

    def __str__(self) -> str:
        return str.format("{:d}\t{}\t{}", self.score, self.target, self.query) + (
            str.format("\t{:s}", self.extra) if self.extra else ""
        )

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Parse the LAST tabular alignment line.

        :param line: LAST tabular alignment line.

        :return: Parsed alignment if `line` properly describes it,
          `None` otherwise.

        """
        parts = str.split(line, "\t", _RECORD_REQUIRED_COLUMNS)
        if len(parts) not in {_RECORD_REQUIRED_COLUMNS, _RECORD_REQUIRED_COLUMNS + 1}:
            return None
        try:
            score = int(parts[0])
        except ValueError:
            return None
        target = Region.of_list(parts[1:6])
        query = Region.of_list(parts[6:11])
        if target is not None and query is not None:
            return Record(
                score,
                target,
                query,
                parts[_RECORD_REQUIRED_COLUMNS]
                if len(parts) > _RECORD_REQUIRED_COLUMNS
                else "",
            )
        return None
