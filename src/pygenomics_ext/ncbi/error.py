"""Exceptions for subpackage `pygenomics_ext.ncbi`."""

from pygenomics_ext import error


class Error(error.Error):
    """Base class for exceptions in subpackage `pygenomics_ext.ncbi`."""
