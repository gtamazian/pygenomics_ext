"""Processing assembly reports by NCBI."""

import itertools
from enum import Enum, auto
from typing import List, NamedTuple, Optional, TypeVar

import pygenomics.common

from pygenomics_ext.ncbi import error


class Error(error.Error):
    """Base class for exceptions related to NCBI assembly reports."""


class SequenceRole(Enum):
    """Values for various roles of a sequence in an assembly."""

    ALT_SCAFFOLD = auto()
    ASSEMBLED_MOLECULE = auto()
    FIX_PATCH = auto()
    NOVEL_PATCH = auto()
    UNLOCALIZED_SCAFFOLD = auto()
    UNPLACED_SCAFFOLD = auto()

    def __str__(self) -> str:
        if self is SequenceRole.ALT_SCAFFOLD:
            return "alt-scaffold"
        if self is SequenceRole.ASSEMBLED_MOLECULE:
            return "assembled-molecule"
        if self is SequenceRole.FIX_PATCH:
            return "fix-patch"
        if self is SequenceRole.NOVEL_PATCH:
            return "novel-patch"
        if self is SequenceRole.UNLOCALIZED_SCAFFOLD:
            return "unlocalized-scaffold"
        assert self is SequenceRole.UNPLACED_SCAFFOLD
        return "unplaced-scaffold"

    @staticmethod
    def of_string(line: str) -> Optional["SequenceRole"]:
        # pylint: disable=too-many-return-statements
        """Parse the sequence role value.

        :param line: Line that encodes a sequence role.

        :return: Parsed sequence role value if `line` properly encodes
          it, `None` otherwise.

        """
        if line == "alt-scaffold":
            return SequenceRole.ALT_SCAFFOLD
        if line == "assembled-molecule":
            return SequenceRole.ASSEMBLED_MOLECULE
        if line == "fix-patch":
            return SequenceRole.FIX_PATCH
        if line == "novel-patch":
            return SequenceRole.NOVEL_PATCH
        if line == "unlocalized-scaffold":
            return SequenceRole.UNLOCALIZED_SCAFFOLD
        if line == "unplaced-scaffold":
            return SequenceRole.UNPLACED_SCAFFOLD
        return None


class MoleculeType(Enum):
    """Type or location if an assigned molecule."""

    CHROMOSOME = auto()
    MITOCHONDRION = auto()

    def __str__(self) -> str:
        if self is MoleculeType.CHROMOSOME:
            return "Chromosome"
        assert self is MoleculeType.MITOCHONDRION
        return "Mitochondrion"

    @staticmethod
    def of_string(line: str) -> Optional["MoleculeType"]:
        """Parse the molecule type or location value.

        :param line: Line that encodes a molecule type or location.

        :return: Parsed molecule type or location if `line` properly
          encodes it, `None` otherwise.

        """
        if line == "Chromosome":
            return MoleculeType.CHROMOSOME
        if line == "Mitochondrion":
            return MoleculeType.MITOCHONDRION
        return None


class AlternativeLocus(NamedTuple):
    """Alternative locus value for an assembly unit."""

    number: int

    def __str__(self) -> str:
        return str.format("ALT_REF_LOCI_{:d}", self.number)

    @staticmethod
    def of_string(line: str) -> Optional["AlternativeLocus"]:
        """Parse the alternative locus name.

        :param line: Line encoding an alternative locus.

        :return: Parsed alternative locus name if `line` properly
          encodes it, `None` otherwise.

        """
        if not str.startswith(line, "ALT_REF_LOCI_"):
            return None

        try:
            number = int(line[len("ALT_REF_LOCI_") :])
        except ValueError:
            return None

        return AlternativeLocus(number)


class AssemblyUnitType(Enum):
    """Types of assembly units."""

    NON_NUCLEAR = auto()
    PATCHES = auto()
    PRIMARY_ASSEMBLY = auto()
    ALT_REF_LOCI = auto()
    HAPLOTIGS = auto()


class IncorrectAltLocusNumber(Error):
    """Incorrect alternative locus number specified."""

    _unit_type: AssemblyUnitType
    _locus_number: Optional[int]

    def __init__(self, unit_type: AssemblyUnitType, locus_number: Optional[int]):
        super().__init__(unit_type, locus_number)
        self._unit_type = unit_type
        self._locus_number = locus_number


class AssemblyUnit:
    """Assembly unit description."""

    _unit_type: AssemblyUnitType
    _alt_locus: Optional[AlternativeLocus]

    def __init__(self, unit_type: AssemblyUnitType, alt_locus_number: Optional[int]):
        self._unit_type = unit_type
        if unit_type is AssemblyUnitType.ALT_REF_LOCI:
            if alt_locus_number is None:
                raise IncorrectAltLocusNumber(unit_type, alt_locus_number)
            self._alt_locus = AlternativeLocus(alt_locus_number)
        else:
            if alt_locus_number is not None:
                raise IncorrectAltLocusNumber(unit_type, alt_locus_number)
            self._alt_locus = None

    @property
    def unit_type(self) -> AssemblyUnitType:
        """Type of an assembly unit."""
        return self._unit_type

    @property
    def alt_locus(self) -> Optional[AlternativeLocus]:
        """Alternative locus object."""
        return self._alt_locus

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, AssemblyUnit):
            return NotImplemented
        if self.unit_type is other.unit_type:
            return (
                self.alt_locus == other.alt_locus
                if self.unit_type is AssemblyUnitType.ALT_REF_LOCI
                else True
            )
        return False

    def __repr__(self) -> str:
        return str.format(
            "{:s}(unit_type={:s}, alt_locus={:s})",
            self.__class__.__name__,
            repr(self.unit_type),
            repr(self.alt_locus),
        )

    def __str__(self) -> str:
        if self.unit_type is AssemblyUnitType.NON_NUCLEAR:
            return "non-nuclear"
        if self.unit_type is AssemblyUnitType.PATCHES:
            return "PATCHES"
        if self.unit_type is AssemblyUnitType.PRIMARY_ASSEMBLY:
            return "Primary Assembly"
        if self.unit_type is AssemblyUnitType.HAPLOTIGS:
            return "haplotigs"
        assert (
            self.unit_type is AssemblyUnitType.ALT_REF_LOCI
            and self.alt_locus is not None
        )
        return str(self.alt_locus)

    @staticmethod
    def of_string(line: str) -> Optional["AssemblyUnit"]:
        """Parse the assembly unit line.

        :param line: Line encoding an assembly unit.

        :return: Parsed assembly unit if `line` properly encodes it,
          `None` otherwise.

        """
        if line == "non-nuclear":
            return AssemblyUnit(AssemblyUnitType.NON_NUCLEAR, None)
        if line == "PATCHES":
            return AssemblyUnit(AssemblyUnitType.PATCHES, None)
        if line == "Primary Assembly":
            return AssemblyUnit(AssemblyUnitType.PRIMARY_ASSEMBLY, None)
        if line == "haplotigs":
            return AssemblyUnit(AssemblyUnitType.HAPLOTIGS, None)
        if str.startswith(line, "ALT_REF_LOCI"):
            parsed_alt_loci = AlternativeLocus.of_string(line)
            return (
                AssemblyUnit(AssemblyUnitType.ALT_REF_LOCI, parsed_alt_loci.number)
                if parsed_alt_loci is not None
                else None
            )
        return None


_T = TypeVar("_T")


def str_optional_value(value: Optional[str]) -> str:
    """Convert an optional value to a string."""
    return value if value is not None else "na"


def parse_optional_value(value: str) -> Optional[str]:
    """Parse an optional value from a string."""
    return value if value != "na" else None


class Sequence(NamedTuple):
    """Assembly sequence record from a report."""

    seq_name: str
    seq_role: SequenceRole
    assigned_molecule: Optional[str]
    molecule_type: Optional[MoleculeType]
    genbank_accn: Optional[str]
    same_seq: bool
    refseq_accn: Optional[str]
    assembly_unit: AssemblyUnit
    seq_length: int
    ucsc_name: Optional[str]

    def __str__(self) -> str:
        return str.format(
            "{:s}\t{}\t{:s}\t{:s}\t{:s}\t{:s}\t{:s}\t{}\t{:d}\t{:s}",
            self.seq_name,
            self.seq_role,
            str_optional_value(self.assigned_molecule),
            pygenomics.common.optional_to_string(self.molecule_type, "na"),
            str_optional_value(self.genbank_accn),
            "=" if self.same_seq else "<>",
            str_optional_value(self.refseq_accn),
            self.assembly_unit,
            self.seq_length,
            str_optional_value(self.ucsc_name),
        )

    @staticmethod
    def of_string(line: str) -> Optional["Sequence"]:
        """Parse an assembly sequence record.

        :param line: String from an NCBI assembly report file encoding
          an assembly sequence.

        :return: Parsed assembly sequence record if `line` properly
          encodes it, `None` otherwise.

        """
        parts = str.split(line, "\t")
        if len(parts) != 10:
            return None

        seq_role = SequenceRole.of_string(parts[1])
        if seq_role is not None:
            assigned_molecule = parse_optional_value(parts[2])
            success_flag, molecule_type = pygenomics.common.optional_from_string(
                MoleculeType.of_string, parts[3], "na"
            )
            if success_flag:
                genbank_accn = parse_optional_value(parts[4])
                if parts[5] in {"<>", "="}:
                    same_seq = parts[5] == "="
                    refseq_accn = parse_optional_value(parts[6])
                    assembly_unit = AssemblyUnit.of_string(parts[7])
                    if assembly_unit is not None:
                        seq_length = pygenomics.common.str_to_int(parts[8])
                        if seq_length is not None:
                            ucsc_name = parse_optional_value(parts[9])
                            return Sequence(
                                parts[0],
                                seq_role,
                                assigned_molecule,
                                molecule_type,
                                genbank_accn,
                                same_seq,
                                refseq_accn,
                                assembly_unit,
                                seq_length,
                                ucsc_name,
                            )
        return None


class IncorrectReportLine(Error):
    """Indicates an incorrect sequence line in an NCBI assembly report."""

    line: str

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


def read(path: str) -> List[Sequence]:
    """Read sequence records from an NCBI assembly report file.

    :note: The function produces a side effect by reading from the
      specified file. It can also raise the exception
      :py:class:`IncorrectReportLine`.

    :param path: NCBI assembly report file name.

    :return: List of sequence records from the specified report file.

    """
    with open(path, encoding="utf-8") as report_file:
        result: List[Sequence] = []
        for line in itertools.dropwhile(lambda x: str.startswith(x, "#"), report_file):
            sequence_record = Sequence.of_string(str.rstrip(line))
            if sequence_record is None:
                raise IncorrectReportLine(line)
            list.append(result, sequence_record)
        return result
