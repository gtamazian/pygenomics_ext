"""Process variant effects annotation in the snpEFF format."""

from enum import Enum, auto
from typing import List, NamedTuple, Optional

from pygenomics import common
from pygenomics.vcf.data import record

from pygenomics_ext import error


class Error(error.Error):
    """Base class for errors related to parsing snpEFF variant effects."""


class ImpactLevel(Enum):
    """Putative impact estimation."""

    HIGH = auto()
    MODERATE = auto()
    LOW = auto()
    MODIFIER = auto()

    def __str__(self) -> str:
        if self is ImpactLevel.HIGH:
            return "HIGH"
        if self is ImpactLevel.MODERATE:
            return "MODERATE"
        if self is ImpactLevel.LOW:
            return "LOW"
        assert self is ImpactLevel.MODIFIER
        return "MODIFIER"

    @staticmethod
    def of_string(line: str) -> Optional["ImpactLevel"]:
        """Parse an impact level value.

        :param line: Line encoding an impact level.

        :return: Parsed impact level value if `line` properly encodes
          it, `None` otherwise.

        """
        if line == "HIGH":
            return ImpactLevel.HIGH
        if line == "MODERATE":
            return ImpactLevel.MODERATE
        if line == "LOW":
            return ImpactLevel.LOW
        if line == "MODIFIER":
            return ImpactLevel.MODIFIER
        return None


class Effect(NamedTuple):
    """Variant effect record for a single allele."""

    allele: str
    effect: List[str]
    impact: ImpactLevel
    gene_name: str
    gene_id: str
    feature_type: str
    feature_id: str
    transcript_biotype: str
    exon_rank: str
    hgvs_dna: str
    hgvs_prot: str
    cdna_pos: str
    cds_pos: str
    prot_pos: str
    distance: str
    extra: str

    def __str__(self) -> str:
        return str.format(
            "{:s}|{:s}|{}" + "|{:s}" * 13,
            self.allele,
            str.join("&", self.effect),
            self.impact,
            self.gene_name,
            self.gene_id,
            self.feature_type,
            self.feature_id,
            self.transcript_biotype,
            self.exon_rank,
            self.hgvs_dna,
            self.hgvs_prot,
            self.cdna_pos,
            self.cds_pos,
            self.prot_pos,
            self.distance,
            self.extra,
        )

    @staticmethod
    def of_string(line: str) -> Optional["Effect"]:
        """Parse a variant effect record.

        :param line: Line encoding a predicted variant effect.

        :return: Parsed variant effect if `line` properly encodes it,
          `None` otherwise.

        """
        parts = str.split(line, "|")
        if len(parts) != 16:
            return None

        effect = str.split(parts[1], "&")
        impact = ImpactLevel.of_string(parts[2])
        if impact is None:
            return None

        return Effect(parts[0], effect, impact, *parts[3:])


class MultipleAnnFields(Error):
    """More than one ANN field in a VCF record."""

    info: record.InfoField

    def __init__(self, info: record.InfoField):
        super().__init__(info)
        self.info = info


class IncorrectAnnField(Error):
    """An incorrect ANN field that could not be parsed."""

    field_value: str

    def __init__(self, field_value: str):
        super().__init__(field_value)
        self.field_value = field_value


def get_effect_annotations(variant: record.Record) -> List[Effect]:
    """Get effect annotations of a VCF variant record.

    :note: The function can raise exceptions
      :py:class:`MultipleAnnFields` or :py:class:`IncorrectAnnField`
      if the effect annotations fail to parse.

    :param variant: VCF variant record.

    :return: List of variant effect annotations; the empty list is
      returned if there are no effect annotations.

    """
    ann_fields = common.assoc_all(variant.info_field.parts, "ANN")
    if not ann_fields:
        return []
    if len(ann_fields) > 1:
        raise MultipleAnnFields(variant.info_field)
    result: List[Effect] = []
    for k in ann_fields[0]:
        parsed_effect = Effect.of_string(k)
        if parsed_effect is None:
            raise IncorrectAnnField(k)
        list.append(result, parsed_effect)
    return result
