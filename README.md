# pygenomics-ext

_Pygenomics-ext_ is an extension package that supplements
[_pygenomics_](https://gitlab.com/gtamazian/pygenomics) with routines
for processing software-specific data formats. _Pygenomics-ext_
uses the same development framework as _pygenomics_.

## Installation

_Pygenomics-ext_ can be installed using _pip_:

```
pip install git+https://gitlab.com/gtamazian/pygenomics-ext.git
```
