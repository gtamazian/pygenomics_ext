"""Test module `juicebox`."""

import string

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext import juicebox

HT_NUMBERED_FEATURES = st.tuples(
    st.sampled_from([juicebox.FeatureKind.FRAGMENT, juicebox.FeatureKind.OVERHANG]),
    st.integers(min_value=0),
).map(lambda x: juicebox.Feature(*x))

HT_NOT_NUMBERED_FEATURES = st.sampled_from(
    [juicebox.FeatureKind.DEBRIS, juicebox.FeatureKind.GAP]
).map(lambda x: juicebox.Feature(x, None))

HT_FEATURES = st.one_of(HT_NUMBERED_FEATURES, HT_NOT_NUMBERED_FEATURES)

HT_FRAGMENTS = st.tuples(
    st.text(alphabet=string.ascii_letters + string.digits, min_size=1),
    st.lists(HT_FEATURES),
    st.integers(min_value=1),
    st.integers(min_value=1),
).map(lambda x: juicebox.Fragment(*x))

HT_PARTS = st.tuples(st.integers(min_value=1), st.booleans()).map(
    lambda x: juicebox.Part(*x)
)

HT_SEQUENCES = st.lists(HT_PARTS)


@ht.given(HT_FRAGMENTS)
def test_fragment_of_string(fragment: juicebox.Fragment) -> None:
    """Test Fragment.of_string() using correct data."""
    assert juicebox.Fragment.of_string(str(fragment)) == fragment


@ht.given(HT_SEQUENCES)
def test_sequence_of_string(sequence: juicebox.Sequence) -> None:
    """Test sequence_of_string() using correct data."""
    assert (
        juicebox.sequence_of_string(juicebox.sequence_to_string(sequence)) == sequence
    )
