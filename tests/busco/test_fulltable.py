"""Test module `busco.fulltable`."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext.busco import fulltable
from tests import common

HT_STRANDS = st.sampled_from(fulltable.Strand)

HT_SEQNAMES = st.text(alphabet=common.PRINTABLE_NON_WHITESPACE, min_size=1, max_size=20)

HT_GENES = st.tuples(
    HT_SEQNAMES,
    st.integers(min_value=0),
    st.integers(min_value=0),
    HT_STRANDS,
    st.integers(min_value=0, max_value=1_000_000),
    st.integers(min_value=1),
    HT_SEQNAMES,
    st.text(
        alphabet=st.characters(blacklist_characters=["\t"]), min_size=1, max_size=20
    ),
).map(lambda x: fulltable.Gene(*x))

HT_MISSING_GENE_RECORDS = HT_SEQNAMES.map(
    lambda x: fulltable.Record(x, fulltable.GeneStatus.MISSING, None)
)

HT_PRESENT_GENE_RECORDS = st.tuples(
    HT_SEQNAMES,
    st.sampled_from(
        [
            fulltable.GeneStatus.COMPLETE,
            fulltable.GeneStatus.DUPLICATED,
            fulltable.GeneStatus.FRAGMENTED,
        ]
    ),
    HT_GENES,
).map(lambda x: fulltable.Record(*x))

HT_GENE_RECORDS = st.one_of(HT_PRESENT_GENE_RECORDS, HT_MISSING_GENE_RECORDS)


@ht.given(HT_GENE_RECORDS)
def test_record_of_string(record: fulltable.Record) -> None:
    """Test Record.of_string() using correct data."""
    assert fulltable.Record.of_string(str(record)) == record
