"""Test module `snpeff`."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext import snpeff
from tests import common

HT_VALUES = st.text(
    alphabet=[k for k in common.PRINTABLE_NON_WHITESPACE if k not in {"|", ",", "&"}],
    min_size=1,
)

HT_IMPACT_LEVELS = st.sampled_from(snpeff.ImpactLevel)

HT_EFFECTS = st.tuples(
    HT_VALUES, st.lists(HT_VALUES, min_size=1), HT_IMPACT_LEVELS, *[HT_VALUES] * 13
).map(lambda x: snpeff.Effect(*x))


@ht.given(HT_EFFECTS)
def test_of_string(effect: snpeff.Effect) -> None:
    """Test effect.of_string() using correct data."""
    assert snpeff.Effect.of_string(str(effect)) == effect
