"""Test module repeatmasker."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext import repeatmasker
from tests import common

HT_SW_SCORES = st.integers()
HT_PERC_VALUES = st.integers(min_value=0, max_value=1_000).map(lambda x: x / 10)
HT_PERCENTAGES = st.tuples(HT_PERC_VALUES, HT_PERC_VALUES, HT_PERC_VALUES).map(
    lambda x: repeatmasker.Percentages(x[0], x[1], x[2])
)
HT_MATCHES = st.tuples(st.integers(), st.integers(), st.integers()).map(
    lambda x: repeatmasker.Match(x[0], x[1], x[2])
)

HT_NAMES = st.text(alphabet=common.PRINTABLE_NON_WHITESPACE, min_size=1, max_size=20)

HT_QUERY_PARTS = st.tuples(HT_NAMES, HT_MATCHES, st.booleans()).map(
    lambda x: repeatmasker.QueryPart(x[0], x[1], x[2])
)

HT_REPEAT_PARTS = st.tuples(
    HT_NAMES, HT_NAMES, st.one_of(HT_NAMES, st.just(None)), HT_MATCHES
).map(lambda x: repeatmasker.RepeatPart(x[0], x[1], x[2], x[3]))

HT_MATCH_IDS = st.one_of(st.integers(min_value=0), st.just(None))

HT_RECORDS = st.tuples(
    HT_SW_SCORES,
    HT_PERCENTAGES,
    HT_QUERY_PARTS,
    HT_REPEAT_PARTS,
    HT_MATCH_IDS,
    st.booleans(),
).map(lambda x: repeatmasker.Record(x[0], x[1], x[2], x[3], x[4], x[5]))


@ht.given(HT_RECORDS)
def test_record_of_string(record: repeatmasker.Record) -> None:
    """Test Record.of_string() using correct RepeatMasker records."""
    assert str(repeatmasker.Record.of_string(str(record))) == str(record)
