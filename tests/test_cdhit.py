"""Test module cdhit."""

import io
from typing import List

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext import cdhit
from tests import common

_MAX_ELEMENTS = 10

HT_NUMBERS = st.integers(min_value=0)

HT_SEQ_UNITS = st.sampled_from(
    [cdhit.SequenceUnitType.AMINOACID, cdhit.SequenceUnitType.NUCLEOTIDE]
)

HT_SEQUENCES = st.tuples(
    HT_NUMBERS,
    st.integers(min_value=1),
    HT_SEQ_UNITS,
    st.text(alphabet=common.PRINTABLE_NON_WHITESPACE, min_size=1, max_size=20),
).map(lambda x: cdhit.Sequence(x[0], x[1], x[2], x[3]))

HT_DIRECTIONS = st.sampled_from(
    [cdhit.HitDirection.POSITIVE, cdhit.HitDirection.NEGATIVE]
)

HT_IDENTITIES = st.integers(min_value=0, max_value=10_000).map(lambda x: x / 100)

HT_ELEMENTS = st.tuples(HT_SEQUENCES, HT_DIRECTIONS, HT_IDENTITIES).map(
    lambda x: cdhit.Element(x[0], x[1], x[2])
)

HT_CLUSTERS = st.tuples(
    HT_NUMBERS, HT_SEQUENCES, st.lists(HT_ELEMENTS, max_size=_MAX_ELEMENTS)
).map(lambda x: cdhit.Cluster(x[0], x[1], x[2]))


@ht.given(st.lists(HT_CLUSTERS))
def test_read_write(clusters: List[cdhit.Cluster]) -> None:
    """Test read() and write()."""
    stream = io.StringIO()
    for k in clusters:
        cdhit.write(stream, k)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    assert all(j == k for j, k in zip(clusters, cdhit.read(io.StringIO(contents))))
