"""Test module fastq.illumina."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext.fastq import illumina
from tests import common

HT_IDS = st.text(
    alphabet=[k for k in common.PRINTABLE_NON_WHITESPACE if k != ":"], min_size=1
)

HT_NON_NEGATIVE_NUMBERS = st.integers(min_value=0)

HT_NUCLEOTIDE_SEQS = st.text(alphabet=["A", "C", "G", "T"], min_size=1)

HT_UMIS = st.one_of(
    st.tuples(HT_NUCLEOTIDE_SEQS, HT_NUCLEOTIDE_SEQS),
    st.just(None),
)

HT_READ_NUMS = st.sampled_from([1, 2])

HT_READ_NAMES = st.tuples(
    HT_IDS,
    HT_NON_NEGATIVE_NUMBERS,
    HT_IDS,
    HT_NON_NEGATIVE_NUMBERS,
    HT_NON_NEGATIVE_NUMBERS,
    HT_NON_NEGATIVE_NUMBERS,
    HT_NON_NEGATIVE_NUMBERS,
    HT_UMIS,
    HT_READ_NUMS,
    st.booleans(),
    HT_NON_NEGATIVE_NUMBERS,
    HT_NUCLEOTIDE_SEQS,
).map(lambda x: illumina.ReadName(*x))


@ht.given(HT_READ_NAMES)
def test_of_string(name: illumina.ReadName) -> None:
    """Test ReadName.of_string() using correct Illumina read names."""
    assert illumina.ReadName.of_string(str(name)) == name
