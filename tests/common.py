"""Common routines for testing pygenomics_ext modules."""

import string

PRINTABLE_NON_WHITESPACE = [
    k for k in string.printable if k not in set(string.whitespace)
]
