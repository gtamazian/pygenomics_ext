"""Test module pygenomics_ext.last.tab."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext.last import tab
from tests.last.common import HT_SEQNAMES, HT_STRANDS

HT_REGIONS = st.tuples(
    HT_SEQNAMES, st.integers(), st.integers(), HT_STRANDS, st.integers()
).map(lambda x: tab.Region(x[0], x[1], x[2], x[3], x[4]))

HT_RECORDS = st.tuples(
    st.integers(), HT_REGIONS, HT_REGIONS, st.text(max_size=100)
).map(lambda x: tab.Record(x[0], x[1], x[2], x[3]))


@ht.given(HT_RECORDS)
def test_of_string(record: tab.Record) -> None:
    """Test Record.of_string()."""
    assert tab.Record.of_string(str(record)) == record
