"""Common routines for testing subpackage pygenomics_ext.last."""

import string

import hypothesis.strategies as st
import pygenomics.strand

HT_STRANDS = st.sampled_from(
    [pygenomics.strand.Strand.PLUS, pygenomics.strand.Strand.MINUS]
)

HT_SEQNAMES = st.text(alphabet=string.ascii_letters, min_size=1, max_size=100)
