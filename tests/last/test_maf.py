"""Test module pygenomics_ext.last.maf."""

import io
import string
from typing import List

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext.last import maf
from tests.last.common import HT_SEQNAMES, HT_STRANDS

HT_ALIGNED_FRAGMENTS = st.tuples(
    HT_SEQNAMES,
    st.integers(),
    st.integers(),
    HT_STRANDS,
    st.integers(),
    st.text(alphabet=string.ascii_letters, min_size=1, max_size=100),
).map(lambda x: maf.AlignedFragment(x[0], x[1], x[2], x[3], x[4], x[5]))

HT_RECORDS = st.tuples(
    st.integers(),
    st.text(alphabet=string.ascii_letters, max_size=100),
    HT_ALIGNED_FRAGMENTS,
    HT_ALIGNED_FRAGMENTS,
).map(lambda x: maf.Record(x[0], x[1], x[2], x[3]))


@ht.given(st.lists(HT_RECORDS))
def test_read_write(records: List[maf.Record]) -> None:
    """Test read() and write()."""
    stream = io.StringIO()
    for k in records:
        maf.write(stream, k)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    assert all(j == k for j, k in zip(records, maf.read(io.StringIO(contents))))
