"""Test module `ncbi.assemblyreport`."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics_ext.ncbi import assemblyreport
from tests import common

HT_SEQ_NAMES = st.text(alphabet=common.PRINTABLE_NON_WHITESPACE, min_size=1).filter(
    lambda x: x != "na"
)

HT_SEQ_ROLES = st.sampled_from(assemblyreport.SequenceRole)

HT_ASSIGNED_MOLECULES = st.one_of(HT_SEQ_NAMES, st.just(None))

HT_MOLECULE_TYPES = st.one_of(
    st.sampled_from(assemblyreport.MoleculeType), st.just(None)
)

HT_GENBANK_ACCNS = st.one_of(HT_SEQ_NAMES, st.just(None))

HT_SAME_SEQS = st.booleans()

HT_REFSEQ_ACCNS = st.one_of(HT_SEQ_NAMES, st.just(None))

HT_ASSEMBLY_UNITS = st.tuples(
    st.sampled_from(assemblyreport.AssemblyUnitType), st.integers(min_value=0)
).map(
    lambda x: assemblyreport.AssemblyUnit(
        x[0], x[1] if x[0] is assemblyreport.AssemblyUnitType.ALT_REF_LOCI else None
    )
)

HT_SEQ_LENGTHS = st.integers(min_value=1)

HT_SEQUENCES = st.tuples(
    HT_SEQ_NAMES,
    HT_SEQ_ROLES,
    HT_ASSIGNED_MOLECULES,
    HT_MOLECULE_TYPES,
    HT_GENBANK_ACCNS,
    HT_SAME_SEQS,
    HT_REFSEQ_ACCNS,
    HT_ASSEMBLY_UNITS,
    HT_SEQ_LENGTHS,
    HT_SEQ_NAMES,
).map(lambda x: assemblyreport.Sequence(*x))


@ht.given(HT_SEQUENCES)
def test_of_string(sequence: assemblyreport.Sequence) -> None:
    """Test Sequence.of_string() using correct data."""
    assert assemblyreport.Sequence.of_string(str(sequence)) == sequence
