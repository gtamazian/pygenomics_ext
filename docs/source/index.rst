.. pygenomics_ext documentation master file, created by
   sphinx-quickstart on Thu Dec  2 20:52:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pygenomics_ext's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Modules <modules.rst>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
