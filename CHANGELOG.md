# Changelog

All notable changes to _pygenomics-ext_ will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.1] - 2023-04-07

### Added

- Add a module for [_BUSCO_](https://busco.ezlab.org) full table files
- Add a module for [_Juicebox_](https://www.dnazoo.org/methods) Hi-C assembly files
- Use [_Poetry_](https://python-poetry.org) for dependency management and packaging
- Use [_tox_](https://tox.wiki) for running tests and static code checks
- Use [_isort_](https://pycqa.github.io/isort/) for formatting import statements
- Use [_flake8_](https://flake8.pycqa.org) for checking code style

### Changed

- Change the project layout from [the "flat layout" to the "src layout"](https://packaging.python.org/en/latest/discussions/src-layout-vs-flat-layout/)
- Move the project configuration to `pyproject.toml` and `tox.ini`
- Call [_radon_](https://radon.readthedocs.io) by [_flake8_](https://flake8.pycqa.org)

### Fixed

- Fix source code formatting

## [0.1.0] - 2022-06-24

### Added

- Initial release of the package

[0.1.1]: https://gitlab.com/gtamazian/pygenomics-ext/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/gtamazian/pygenomics-ext/-/tags/0.1.0
